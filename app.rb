require 'sinatra/base'
require 'sinatra/json'
require 'json'

class App < Sinatra::Base
  get '/' do
    erb :index
  end

  get '/resume/:name.json' do
    #send_file 'resumes/%s.json' % params['name'], type: :json, disposition: :inline
    begin
      json ResumeStore.new(JsonFile.new).find(params['name']) #Just to simulate, it could use a database as well.
    rescue
      halt(404)
    end
  end

  not_found {"404 happened!"}
end

class ResumeStore
  def initialize(storage)
    @storage = storage
  end

  def find(name)
    @storage.select(name)
  end
end

class JsonFile
  def select(name)
    JSON.parse(File.read("resumes/%s.json" % name))
  end
end
