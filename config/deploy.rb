set :user, 'babar'

server 'ruby.ibabar.com', user: fetch(:user), roles: %w{web app}, ssh_options: { port: 6395 }

set :application, 'json-resume'
set :repo_url, 'git@gitlab.com:babar/json-resume.git'

set :deploy_to, "/home/#{fetch(:user)}/Sites/#{fetch(:application)}"
set :scm, :git

set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/sockets')

set :log_level, :info

namespace :deploy do
  def template(from, to = shared_path)
    template_path = "config/#{from}.erb"
    template = ERB.new(File.new(template_path).read).result(binding)
    upload! StringIO.new(template), "#{to}/#{from}"
  end

  desc 'Upload NginX Config'
  task :nginx_conf do
    on roles(:web) do |role|
      template 'nginx.conf'
      execute :sudo, 'service nginx restart'
    end
  end
end
